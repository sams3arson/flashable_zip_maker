#!/bin/bash

# Flashable zip maker from GSI (boot.img + system.img, in our case, for UBPorts) for CI by @sams3arson

set -xe

echo "Flashable zip maker (@sams3arson) started..."

OUT=$1

[ ! -z "$OUT" ] && [ -d "$OUT" ] || (echo "out directory is not passed or doesn't exist, exiting..." && exit -1)

[ -f "$OUT/system.img" ] && [ -f "$OUT/boot.img" ] || (echo "Either system.img or boot.img not found in provided out directory, exiting.." && exit -1)

cd $OUT
OUT=$(pwd -P)

TMP=$(mktemp -d) && cd "$TMP"

git clone https://github.com/notmyst33d/gsi2zip.git && git clone https://gitlab.com/sams3arson/ubports_RMX2020_zip.git && rm -rf ubports_RMX2020_zip/.git

GSI2ZIP="$TMP/gsi2zip"
FLASHZIP="$TMP/ubports_RMX2020_zip"
IMAGES="$TMP/preparing_images"

mkdir "$IMAGES" && cd "$IMAGES"

python3 "$GSI2ZIP/img2sdat/img2sdat.py" -v 4 "$OUT/system.img"

pwd -P && ls -la

[ -f "system.new.dat" ] && [ -f "system.patch.dat" ] && [ -f "system.transfer.list" ] || (echo "img2sdat has been run, but not all required files are present, exiting..." && exit -1)

brotli -6 "system.new.dat"
pwd -P && ls -la

[ -f "system.new.dat.br" ] || (echo "brotli has been run, but the compressed system image can't be found, exiting..." && exit -1)

cd "$TMP"

cp "$IMAGES/system.new.dat.br" "$IMAGES/system.patch.dat" "$IMAGES/system.transfer.list" "$OUT/boot.img" "$FLASHZIP"

cd "$FLASHZIP" && zip -r "$TMP/ubports_RMX2020_flashable.zip" *

cp "$TMP/ubports_RMX2020_flashable.zip" "$OUT"

echo "Flashable zip maker (@sams3arson) finished."
